from tkinter import filedialog
from tkinter import *
import os
import datetime
import sys
from tkinter import ttk
from collections import Counter
import shutil


class Listing:
    def __init__(self,master,st=0):
        self.master=master
        master.title('Move skipped TIF and TFW to new folder')
        #menu
        self.label_1 = Label(master, text="Folder with trajectories: ")
        self.label_1.grid(row=0, sticky="E")
        self.entry_1=Entry(master, width=60)
        self.entry_1.insert(END,'')
        self.entry_1.grid(row=0,column=1, sticky="E")
        self.label_2 = Label(master, text="")
        self.label_2.grid(row=1, column=3)

        self.label_2 = Label(master, text="Image skip: ")
        self.label_2.grid(row=1, sticky="E")
        self.entry_2=Entry(master, width=5)
        self.entry_2.insert(END,'2')
        self.entry_2.grid(row=1,column=1, sticky="W")
        self.label_3 = Label(master, text="")
        self.label_3.grid(row=1, column=3)

        self.label_3 = Label(master, text="Cameras folders: ")
        self.label_3.grid(row=2, sticky="E")
        self.entry_3=Entry(master, width=5)
        self.entry_3.insert(END,'0,2')
        self.entry_3.grid(row=2,column=1, sticky="W")
        self.label_4 = Label(master, text="")
        self.label_4.grid(row=2, column=3)
        
        self.button_1 = Button(master, text="Get path", command=self.direction, height=1,width=10)
        self.button_1.grid(row=0,column=2, sticky="E")

        self.v=IntVar()
        self.Radiobutton=Radiobutton(master, text="Move_files", variable=self.v, value=1).grid(row=2, column=1)
        self.Radiobutton=Radiobutton(master, text="Copy_files", variable=self.v, value=2).grid(row=2, column=1, sticky="e")
        self.v.set(1)

        self.w=IntVar()
        self.Checkbutton=Checkbutton(master, text="Add folder name to files", variable=self.w).grid(row=3,column=1, sticky="W")
        self.w.set(1)

        self.start_button = Button(master, text="Start", command=self.main, height=1,width=10)
        self.start_button.grid(row=6,column=2)
        
        self.close_button = Button(master, text="Exit", command=root.destroy, height=1,width=10)
        self.close_button.grid(row=6,column=0)

        self.label_5 = Label(master, text="", fg="red",font="Verdana 10 bold")
        self.label_5.grid(row=5, column=1)         


    def get_dirs(self,path):  #get unique folders with camera name
        self.path=path
        file=[]
        for root, dirs, files in os.walk(self.path):
            file.append(root)
        out=[]
        cam=self.entry_3.get()
        cam=cam.split(',')
        for i in file:
            for j in cam:
                if ("camera "+str(j)) in i:
                    out.append(i)
        return out

    def MSG(self, message):
        self.message=message
        self.master.update()
        self.label_5['text']=message

    def mover(self,source_path, path, offset):
        self.source_path=path
        self.path=path
        self.offset=offset
        
        #create new directories for files
        new_path=path[:path.rfind("camera")-1]
        new_path=new_path[new_path.rfind("\\")+1:]
        camera=path[path.find("camera"):]        
        new_path=source_path+"\\Selected_"+offset+"__"+new_path
        if not os.path.exists(new_path):
            os.mkdir(new_path)
        if not os.path.exists(new_path+"\\"+camera):
            os.mkdir(new_path+"\\"+camera)

        #list of file in folder
        extensions = [".tif", ".tfw"]
        file_list=[]

        for root, dirs, files in os.walk(path): #list of files in folder
            for name in files:
               filename, file_extension = os.path.splitext(name)
               if file_extension in extensions[0]:
                   file_list.append(os.path.join(root, filename))

        
        for i in range (0,len(file_list),int(offset)):
            for j in range (0,len(extensions)):            
                try:
                    
                    if self.w.get()==1:#change name of file if checkbox set
                        fold=file_list[i].replace(self.entry_1.get(),'') 
                        fold=fold[fold.find("\\")+1:]
                        fold=fold[:fold.find("\\")]
                        data=fold+"_"+file_list[i][file_list[i].rfind("\\")+1:]
                    else:
                        data=file_list[i][file_list[i].rfind("\\"):]
                        
                    if self.v.get()==1: #move or copy data
                        os.rename(str(file_list[i])+str(extensions[j]),str(new_path)+"\\"+str(camera)+"\\"+data+str(extensions[j]))
                    else:
                        shutil.copy(str(file_list[i])+str(extensions[j]),str(new_path)+"\\"+str(camera)+"\\"+data+str(extensions[j]))
                except OSError:
                    print("Problem with file "+file_list[i]+extensions[j])
                              
    def direction(self):
        window=Tk()
        window.withdraw()
        window.title("Trajectories Folder")
        source_path =  filedialog.askdirectory(parent=window,initialdir=os.getcwd(),title='Please select a directory')
        source_path=source_path.replace("/","\\")
        entry=self.entry_1.get()
    
            
        if not source_path:
            if not source_path and not entry:
                self.MSG("Select path!")
            return
        else:
            self.entry_1.delete(0,END)
            self.entry_1.insert(0,source_path)
            self.MSG('')


    def main(self):
        offset=self.entry_2.get()                #get offset     
        uni_dirs=self.get_dirs(self.entry_1.get()) #get unique cameras folders
        if not uni_dirs:
            self.MSG("No folder with selected cameras haven't found")
            sys.exit("")
        for i in range(0,len(uni_dirs)):

            self.MSG("Processing "+str(i+1)+"/"+str(len(uni_dirs))+" folders    "+str(round(((i+1)/len(uni_dirs))*100,1))+"%")
            self.mover(self.entry_1.get(),uni_dirs[i],offset)
        
        self.MSG("Process ends")


root=Tk()
bar=Listing(root)
root.mainloop()





